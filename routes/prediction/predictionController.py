from fastapi import  APIRouter

from model.predictionModel import TeamPlaying
from services.predictionService import predict_winner

router = APIRouter(prefix="/predict")

@router.post("/")
def getPrediction(playing: TeamPlaying):
    return predict_winner(playing)
